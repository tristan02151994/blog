package BlogApp;

import java.util.Date;

public class ArticlesPOJO {
    private Integer id;
    private String title;
    private String content;
    private Date dateCreated;
    private Integer authorId;

    public ArticlesPOJO(Integer id, String title, String content, Date dateCreated, Integer authorId) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.dateCreated = dateCreated;
        this.authorId = authorId;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }
}